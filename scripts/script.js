
const switchTheme = document.querySelector('#cbx-3');
const swtchBacground = document.querySelectorAll('.switcher')


console.log(localStorage)
window.onload = checkTheme();
    function checkTheme() {
        const checkedTheme = localStorage.getItem('theme');
        if (checkedTheme !== null && checkedTheme === 'dark') {
            swtchBacground.forEach(item => {
                item.classList.add('dark')
                switchTheme.setAttribute('checked', 'true')
            })
        } else {
            swtchBacground.forEach(item => {
                item.classList.add('light')
                switchTheme.removeAttribute('checked')
            })
        }
    }

    switchTheme.addEventListener('click', () => {
        if (switchTheme.checked === false) {
            swtchBacground.forEach(item => {
                item.classList.add('light')
                localStorage.setItem('theme', 'light')
                switchTheme.setAttribute('checked', 'false')
            })
        } else {
            swtchBacground.forEach(item => {
                item.classList.remove('light')
                localStorage.setItem('theme', 'dark')
                switchTheme.setAttribute('checked', 'true')
            })
        }
    })
